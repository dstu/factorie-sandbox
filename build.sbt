name := "FACTORIE sandbox"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.9.1"

resolvers += "UMASS releases" at "http://iesl.cs.umass.edu:8081/nexus/content/repositories/releases/"

libraryDependencies += "org.scalatest" %% "scalatest" % "1.6.1" % "test"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "6.0.3"

libraryDependencies += "cc.factorie" % "factorie" % "0.9.2"

scalacOptions += "-deprecation"

scalacOptions += "-unchecked"
